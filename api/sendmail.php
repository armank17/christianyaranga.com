<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

$rest_json = file_get_contents("php://input");
$_POST = json_decode($rest_json, true);

$form = $_POST['data'];


try {
    //Server settings
    //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
    $mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = 'smtp-relay.gmail.com';                    // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'administrador@somoswebs.com';                     // SMTP username
    $mail->Password   = 'printarmank17';                               // SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom($form['email'], $form['firstname']);
    $mail->addAddress('administrador@somoswebs.com', 'Mailer');     // Add a recipient


    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Conocer planes y precios';
    $body = '<b>Pais:</b> '.$form['pais_name'].'<br>';
    $body .= '<b>Nombre:</b> '.$form['firstname'].'<br>';
    $body .= '<b>Apellidos:</b> '.$form['lastname'].'<br>';
    $body .= '<b>Correo:</b> '.$form['email'].'<br>';
    $body .= '<b>Departamento:</b> '.$form['departament'].'<br>';
    $body .= '<b>Teléfono:</b> '.$form['phone_number'].'<br>';
    $body .= '<b>Edad:</b> '.$form['age'].'<br>';
    
    $mail->Body = $body;

    $mail->send();
    echo json_encode(
        [
            "sent" => true,
            "message" => 'Message has been sent',
        ]
    );
   
} catch (Exception $e) {
    echo json_encode(
        [
           "sent" => false,
           "message" => 'error',
        ]
    );

}