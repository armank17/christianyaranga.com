import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {
  Grid,
  Typography,
  isWidthUp,
  withWidth,
  withStyles
} from "@material-ui/core";
import PriceCard from "./PriceCard";
import calculateSpacing from "./calculateSpacing";

const styles = theme => ({
  containerFix: {
    [theme.breakpoints.down("md")]: {
      paddingLeft: theme.spacing(6),
      paddingRight: theme.spacing(6)
    },
    [theme.breakpoints.down("sm")]: {
      paddingLeft: theme.spacing(4),
      paddingRight: theme.spacing(4)
    },
    [theme.breakpoints.down("xs")]: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2)
    },
    overflow: "hidden",
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  },
  cardWrapper: {
    [theme.breakpoints.down("xs")]: {
      marginLeft: "auto",
      marginRight: "auto",
      maxWidth: 340
    }
  },
  cardWrapperHighlighted: {
    [theme.breakpoints.down("xs")]: {
      marginLeft: "auto",
      marginRight: "auto",
      maxWidth: 360
    }
  },
  Love: {
    fontFamily: "'Love Ya Like A Sister', cursive",
    fontWeight: 400
  },
  BreeSerif: {
    fontFamily: "'Bree Serif', cursive",
    fontWeight: 400
  },
  LuckiestGuy: {
    fontFamily: "'Luckiest Guy', cursive",
    fontWeight: 400
  },
});

function PricingSection(props) {
  const { width, classes } = props;
  return (
    <div id="precios" className="lg-p-top" style={{ backgroundColor: "#FFFFFF" }}>
      <Typography variant="h3" align="center" className={classNames(classes.Love, "lg-mg-bottom")} >
        Precios
      </Typography>
      <div className={classNames("container-fluid", classes.containerFix)}>
        <Grid
          container
          spacing={calculateSpacing(width)}
          className={classes.gridContainer}
        >
          <Grid
            item
            xs={12}
            sm={6}
            lg={3}
            className={classes.cardWrapper}
            data-aos="zoom-in-up"
          >
            <PriceCard
              title={<Typography align="center" variant="h3"  className={classes.BreeSerif}>Básico</Typography>}
              pricing={
                <span>
                  <span className={classes.BreeSerif}>S/.250</span>
                  <Typography display="inline"> / desde</Typography>
                </span>
              }
              features={["Diseño standar moderno", "Diseño responsivo", "Hasta 3 secciones", "Texto e imágenes", "Dominio y hosting incluido", "Soporte 1 mes"]}
            />
          </Grid>
          <Grid
            item
            className={classes.cardWrapperHighlighted}
            xs={12}
            sm={6}
            lg={3}
            data-aos="zoom-in-up"
            data-aos-delay="200"
          >
            <PriceCard
              highlighted
              title={<Typography align="center" variant="h3" className={classes.BreeSerif}>Estandar</Typography>}
              pricing={
                <span>
                  <span className={classes.BreeSerif}>S/.350</span>
                  <Typography display="inline"> / desde</Typography>
                </span>
              }
              features={["Diseño standar",  "Diseño responsivo", "Hasta 4 secciones", "Correos corporativos", "Formulario de contacto", "Soporte 1 mes"]}
            />
          </Grid>
          <Grid
            item
            className={classes.cardWrapper}
            xs={12}
            sm={6}
            lg={3}
            data-aos="zoom-in-up"
            data-aos-delay={isWidthUp("md", width) ? "400" : "0"}
          >
            <PriceCard
              title={<Typography align="center" variant="h3" className={classes.BreeSerif}>Premium</Typography>}
              pricing={
                <span>
                  <span className={classes.BreeSerif}>S/.600</span>
                  <Typography display="inline"> / desde</Typography>
                </span>
              }
              features={["Diseño personalizado", "Diseño responsivo", "Hasta 5 secciones", "Correos corporativos", "Formulario de contacto", "Soporte 3 meses"]}
            />
          </Grid>
          <Grid
            item
            className={classes.cardWrapper}
            xs={12}
            sm={6}
            lg={3}
            data-aos="zoom-in-up"
            data-aos-delay={isWidthUp("md", width) ? "600" : "200"}
          >
            <PriceCard
              title={<Typography align="center" variant="h3"  className={classes.BreeSerif}>Business</Typography>}
              pricing={
                <span>
                  <span className={classes.BreeSerif}>S/.1,500</span>
                  <Typography display="inline"> / desde</Typography>
                </span>
              }
              features={["Análisis del negocio", "Branding", "Diseño profesional", "Diseño responsivo", "Google analytics", "Soporte 3 meses"]}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
}

PricingSection.propTypes = {
  width: PropTypes.string.isRequired
};

export default withStyles(styles, { withTheme: true })(
  withWidth()(PricingSection)
);
