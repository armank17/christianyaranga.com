import React from "react";
import PropTypes from "prop-types";
import { Card, CardContent, CardHeader, Avatar, Divider, Typography, Box, withStyles } from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";


const styles = theme => ({
  card: {
    paddingTop: theme.spacing(6),
    paddingBottom: theme.spacing(6),
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    marginTop: theme.spacing(2),
    border: `3px solid ${theme.palette.primary.dark}`,
    borderRadius: theme.shape.borderRadius * 2,
    textAlign: 'center',
  },
  cardHightlighted: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(4),
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    border: `3px solid ${theme.palette.primary.dark}`,
    borderRadius: theme.shape.borderRadius * 2,
    backgroundColor: theme.palette.primary.main,
    [theme.breakpoints.down("xs")]: {
      marginTop: theme.spacing(2)
    }
  },
  title: {
    color: theme.palette.primary.main
  },

  avatar: {
    width: 60,
    height: 60,
    margin: 'auto',
    backgroundColor: theme.palette.primary.main,
    
  },
  heading: {
    fontSize: 18,
    fontWeight: 'bold',
    letterSpacing: '0.5px',
    marginTop: 8,
    marginBottom: 0,
  },
  subheader: {
    fontSize: 14,
    color: theme.palette.grey[500],
    marginBottom: '0.875em',
  },
  statLabel: {
    fontSize: 12,
    color: theme.palette.grey[500],
    fontWeight: 500,
    fontFamily:
      '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
    margin: 0,
  },
  statValue: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 4,
    letterSpacing: '1px',
  },
  BreeSerif: {
    paddingBottom: theme.spacing(2),
  }
});

function PriceCard(props) {
  const { classes, theme, title, pricing, features, highlighted } = props;

  

  return (
    <Card  className={highlighted ? classes.cardHightlighted : classes.card}>
      



    <CardContent>
      <Typography pb={5} align="center"  variant="h3" color="primary" className={classes.BreeSerif}>Paquete de</Typography>
      <Avatar className={classes.avatar}>{title}</Avatar>
        
      <span className={classes.subheader}>horas</span>
      </CardContent>
      {features.map((feature, index) => (
      <>
      <Divider light />
      <Box display={''}>
      
        <Box p={2} flex={'auto'} className={""}>
          <p className={classes.statLabel}>{feature.subtitle}</p>
          <p className={classes.statValue}>{feature.title}</p>
        </Box>
     
      </Box>
      </>
       ))}

    </Card >
  );
}

PriceCard.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  pricing: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
  highlighted: PropTypes.bool
};

export default withStyles(styles, { withTheme: true })(PriceCard);
