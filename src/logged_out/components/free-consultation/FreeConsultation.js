import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import FcBannerSection from "./FC_BannerSection";
import FcSegundaSection from "./FC_SegundaSection";
import PricingSection from "./PricingSection";

function FreeConsultation(props) {
  const { selectFreeConsultation } = props;
  useEffect(() => {
    selectFreeConsultation();
  }, [selectFreeConsultation]);
  return (
    <Fragment>
      <FcBannerSection />
      <FcSegundaSection />
      
    </Fragment>
  );
}

FreeConsultation.propTypes = {
  selectFreeConsultation: PropTypes.func.isRequired
};

export default FreeConsultation;