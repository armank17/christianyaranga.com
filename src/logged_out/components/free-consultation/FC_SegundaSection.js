import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Grid, Typography, isWidthUp, withWidth, withStyles, Box } from "@material-ui/core";
import PriceCard from "./PriceCard";
import calculateSpacing from "./calculateSpacing";
import imageHome3 from "../../dummy_data/images/imageHome3.jpg";
const styles = theme => ({
  containerFix: {
    [theme.breakpoints.down("md")]: {
      //paddingLeft: theme.spacing(6),
      //paddingRight: theme.spacing(6)
    }
  },
  image: {
    maxWidth: "100%",
    verticalAlign: "middle",
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.shadows[4]
  },
  Love: {
    fontFamily: "'Love Ya Like A Sister', cursive",
    fontWeight: 400
  },
});

function CAI_BannerSection(props) {
  const { width, classes} = props;
  return (
    <div style={{ backgroundColor: "#FFFFFF" }}>
      <div className="container-fluid lg-p-top lg-p-bottom">
        <Box mb={5}>
          <Typography 
          variant="h3" 
          align="center" 
          >
          Planes
          </Typography>
        </Box>
        
        <div className={classNames("container-fluid", classes.containerFix)}>
        <Grid
          container
          spacing={calculateSpacing(width)}
          className={classes.gridContainer}
        >
          <Grid
            item
            xs={12}
            sm={6}
            lg={4}
            className={classes.cardWrapper}
            data-aos="zoom-in-up"
          >
            <PriceCard
              title={<Typography align="center" variant="h4"  className={classes.BreeSerif}>15</Typography>}
              pricing={
                <span>
                  <span className={classes.BreeSerif}>S/.250</span>
                  <Typography display="inline"> / desde</Typography>
                </span>
              }
              features={[ {title: "S/ 1,050", subtitle: "Precio regular"}, {title: "No aplica", subtitle: "Descuento"}, {title: "Ninguno", subtitle: "Ahorro"}, {title: "s/ 70", subtitle: "Costo por hora"} ]}
            />
          </Grid>
       
          <Grid
            item
            className={classes.cardWrapper}
            xs={12}
            sm={6}
            lg={4}
            data-aos="zoom-in-up"
            data-aos-delay={isWidthUp("md", width) ? "400" : "0"}
          >
            <PriceCard
              title={<Typography align="center" variant="h4"  className={classes.BreeSerif}>30</Typography>}
              pricing={
                <span>
                  <span className={classes.BreeSerif}>S/.600</span>
                  <Typography display="inline"> / desde</Typography>
                </span>
              }
              features={[ {title: "S/ 2,100", subtitle: "Precio regular"}, {title: "-10% = s/ 1,890", subtitle: "Descuento"}, {title: "s/ 210", subtitle: "Descuento"}, {title: "s/ 63", subtitle: "Costo por hora"} ]}
            />
          </Grid>
          <Grid
            item
            className={classes.cardWrapper}
            xs={12}
            sm={6}
            lg={4}
            data-aos="zoom-in-up"
            data-aos-delay={isWidthUp("md", width) ? "600" : "200"}
          >
            <PriceCard
              title={<Typography align="center" variant="h4"  className={classes.BreeSerif}>60</Typography>}
              pricing={
                <span>
                  <span className={classes.BreeSerif}>S/.1,500</span>
                  <Typography display="inline"> / desde</Typography>
                </span>
              }
              features={[ {title: "S/ 4,200", subtitle:"Precio regular"}, {title: "-15% = s/ 3,570", subtitle: "Descuento"}, {title: "s/ 630", subtitle: "Descuento"}, {title: "s/ 59.50", subtitle: "Costo por hora"} ]}
            />
          </Grid>

          <Grid
                  item
                  xs={12}
                  sm={12}
                  lg={12}
                  className={classes.cardWrapper}
                  data-aos="zoom-in-up"
                >
                  
                  
                  <Box mt={5}> 
                    <Typography paragraph color="body" align="center">
                    “Los precios NO incluyen IGV y son referenciales ya que pueden variar en función de la cantidad de alumnos por grupo.”                    </Typography>
                  </Box>
                </Grid>
               

        </Grid>


        
        </div>
      </div>
    </div>
  );
}

CAI_BannerSection.propTypes = {
  width: PropTypes.string.isRequired
};


export default withStyles(styles, { withTheme: true })(
  withWidth()(CAI_BannerSection)
);
