import React, { memo } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { Box } from "@material-ui/core";
import AnchorLink from 'react-anchor-link-smooth-scroll';
import LogoImage from "../../dummy_data/images/logo.png";
import {
  AppBar,
  Toolbar,
  Button,
  Hidden,
  IconButton,
  withStyles
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import LabelIcon from "@material-ui/icons/Label";
import PhoneAppIcon from "@material-ui/icons/Phone";
//import MoneyIcon from "@material-ui/icons/";

import NavigationDrawer from "../../../shared/components/NavigationDrawer";
import classNames from "classnames";
const styles = theme => ({
  appBar: {
    boxShadow: theme.shadows[6],
    backgroundColor: theme.palette.common.white,
  },
  bgMain: {
    backgroundColor:"#fbba10",
  },
  toolbar: {
    display: "flex",
    justifyContent: "space-between",
  },
  toolbar2: {
    display: "flex",
    minHeight: "auto",
    //justifyContent: "space-between"
  },
  menuButtonText: {
    fontSize: theme.typography.body1.fontSize,
    fontWeight: theme.typography.h6.fontWeight
  },
  LuckiestGuy: {
    fontFamily: "'Luckiest Guy', cursive",
    fontWeight: 400
  },
  noDecoration: {
    textDecoration: "none !important"
  },
  toolbarH: {
    minHeight: 111,
    [theme.breakpoints.up("xs")]: {
      minHeight: 56,
    },
    [theme.breakpoints.up("sm")]: {
     
    },
    [theme.breakpoints.up("md")]: {
      minHeight: 111,
     
    },
    [theme.breakpoints.up("lg")]: {
     
    },
    [theme.breakpoints.down("lg")]: {
      
    },
  }
});

function NavBar(props) {
  const {
    classes,
    handleMobileDrawerOpen,
    handleMobileDrawerClose,
    mobileDrawerOpen,
    selectedTab
  } = props;
  const menuItems = [
    /*{
      link: "/home",
      name: "INICIO",
      icon: <HomeIcon className="text-white" />,
      onePage: false
    },*/
    {
      link: "/como-aprender-ingles",
      name: "Cómo funciona",
      icon: <LabelIcon className="text-white" />,
      onePage: false
    },
    {
      link: "/cursos-de-ingles",
      name: "Lo que tiene el curso",
      icon: <LabelIcon className="text-white" />,
      onePage: false
    },
    /*{
      link: "/free-consultation",
      name: "Planes",
      icon: <LabelIcon className="text-white" />,
      onePage: false
    },*/
    /*{
      link: "/blog",
      name: "NUESTRA EXPERIENCIA",
      icon: <BookIcon className="text-white" />,
      onePage: false
    },
    {
      link: "#contactenos",
      name: "CONTÁCTENOS",
      icon: <WhatsAppIcon className="text-white" />,
      onePage: true
    },
    */
    /*
    {
      name: "Register",
      onClick: openRegisterDialog,
      icon: <HowToRegIcon className="text-white" />
    },
    {
      name: "Login",
      onClick: openLoginDialog,
      icon: <LockOpenIcon className="text-white" />
    }*/
  ];
  return (
    <div className={classNames(classes.root, classes.toolbarH)}>
      <AppBar position="fixed" className={classes.appBar}>
        <div className={classNames(classes.bgMain)}>
        <Toolbar className={classNames("container-fluid", classes.toolbar)}>
          
          <Link
            key="home"
            to="/"
            className={classes.noDecoration}
            onClick={handleMobileDrawerClose}
          >
            <img
              src={LogoImage}
              className={classes.image}
              alt="header example"
            />
          </Link>
          <Hidden mdUp>
            <IconButton
              className={classes.menuButton}
              onClick={handleMobileDrawerOpen}
              aria-label="Open Navigation"
            >
              <MenuIcon color="secondary" />
            </IconButton>
          </Hidden>
          <Hidden smDown>
            <Button
              color="secondary"
              size="large"
              classes={{ text: classes.menuButtonText }}
              startIcon={<PhoneAppIcon className="text-black" />}
              href="tel:+51920011775"
            >
            +51 920 011 775
            </Button>
          </Hidden>
            
        </Toolbar>
        </div>
        <Hidden smDown>
          <Box borderColor="primary.main" border={1} borderLeft={0} borderRight={0}>
            <Toolbar className={classNames("container-fluid", classes.toolbar2)} >
                {menuItems.map(element => {
                  if (element.link && !element.onePage) {
                    return (
                      <Link
                        key={element.name}
                        to={element.link}
                        className={classes.noDecoration}
                        onClick={handleMobileDrawerClose}
                      >
                        <Button
                          color="secondary"
                          size="large"
                          classes={{ text: classes.menuButtonText }}
                        >
                          {element.name}
                        </Button>
                      </Link>
                    );
                  }else{
                    return (
                    <AnchorLink 
                      href={element.link}
                      key={element.name}
                      className={classes.noDecoration}
                      onClick={handleMobileDrawerClose}
                    >
                    
                      <Button
                        color="secondary"
                        size="large"
                        classes={{ text: classes.menuButtonText }}
                      > 
                        {element.name}
                      </Button>
                    </AnchorLink>
                    );
                  }
                  /*return (
                    <Button
                      color="secondary"
                      size="large"
                      onClick={element.onClick}
                      classes={{ text: classes.menuButtonText }}
                      key={element.name}
                    >
                      {element.name}
                    </Button>
                  );*/
                })}
            </Toolbar>
          </Box>
          
        </Hidden>
        
      </AppBar>
      <Toolbar id="back-to-top-anchor" />
      <NavigationDrawer
        menuItems={menuItems}
        anchor="right"
        open={mobileDrawerOpen}
        selectedItem={selectedTab}
        onClose={handleMobileDrawerClose}
      />
    </div>
  );
}

NavBar.propTypes = {
  classes: PropTypes.object.isRequired,
  handleMobileDrawerOpen: PropTypes.func,
  handleMobileDrawerClose: PropTypes.func,
  mobileDrawerOpen: PropTypes.bool,
  selectedTab: PropTypes.string,
  openRegisterDialog: PropTypes.func.isRequired,
  openLoginDialog: PropTypes.func.isRequired
};

export default withStyles(styles, { withTheme: true })(memo(NavBar));
