import React, { memo } from "react";
import PropTypes from "prop-types";
import { Switch } from "react-router-dom";
import PropsRoute from "../../shared/components/PropsRoute";
import Home from "./home/Home";
import ComoAprenderIngles from "./como-aprender-ingles/ComoAprenderIngles";
import CursosDeIngles from "./cursos-de-ingles/CursosDeIngles";
import FreeConsultation from "./free-consultation/FreeConsultation";

function Routing(props) {
  const { selectHome, selectComoAprenderIngles, selectCursosDeIngles, selectFreeConsultation } = props;
  return (
    <Switch>

      
      <PropsRoute
        exact
        path="/como-aprender-ingles"
        component={ComoAprenderIngles}
        selectComoAprenderIngles={selectComoAprenderIngles}
        
      />
      <PropsRoute
        exact
        path="/cursos-de-ingles"
        component={CursosDeIngles}
        selectCursosDeIngles={selectCursosDeIngles}
        
      />
      
      {/*<PropsRoute
        exact
        path="/free-consultation"
        component={FreeConsultation}
        selectFreeConsultation={selectFreeConsultation}
        
      />*/}
      
      )
      <PropsRoute path="/" component={Home} selectHome={selectHome} />)
    </Switch>
  );
}

Routing.propTypes = {
  
  selectHome: PropTypes.func.isRequired,
  
};

export default memo(Routing);
