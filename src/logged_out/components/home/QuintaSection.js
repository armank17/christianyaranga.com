import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Grid, Typography, isWidthUp, withWidth, withStyles, Card, CardContent, CardActions, Box } from "@material-ui/core";

import calculateSpacing from "./calculateSpacing";
import RobertoImage from "../../dummy_data/images/mx-1234_roberto.png";
import FormatQuoteIcon from '@material-ui/icons/FormatQuote';
import { fontSize } from "@material-ui/system";

const styles = theme => ({
  containerFix: {
    [theme.breakpoints.down("md")]: {
      paddingLeft: theme.spacing(6),
      paddingRight: theme.spacing(6)
    }
  },
  gridContainer:{
    display: "flex",
    justifyContent: "space-between",
  },
  image: {
    maxWidth: "100%",
    verticalAlign: "middle",
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.shadows[4]
  },
  Love: {
    fontFamily: "'Love Ya Like A Sister', cursive",
    fontWeight: 400
  },
  bgColor: {
    backgroundColor: "#F7F5F0",
  },
  bgBanner: {
    background: `url( ${RobertoImage} )`,
    backgroundPosition: "50% bottom",
    //backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    //bottom: "auto",
  },
  content: {
    height: "300px",
    //paddingBottom: "40px",
  },
  content2: {
    height: "130px",
    //paddingBottom: "40px",
  },
  sizeIcon: {
    fontSize: theme.spacing(10),
  }
});

function QuintaSection(props) {
  const { width, classes} = props;
  return (
    <div style={{ backgroundColor: "#F7F5F0" }} className={classNames( "")}>
      <div className="container-fluid lg-p-top lg-p-bottom">
        
        <div className={classNames("container-fluid", classes.container)}>
        <Grid
          container
          spacing={calculateSpacing(width)}
        >
          <Grid item xs={12} sm={12} md={8} className={classes.cardWrapper} 
       
          >

          <Grid
            container
            spacing={calculateSpacing(width)}
            className={classes.gridContainer}
          >
            
          <Grid item xs={12} sm={12} lg={12} data-aos="zoom-in-up" >
            <Box pb={5}>
              <Typography variant="h5" align="center" className={classNames(classes.mb0)}>
              Mira algunas opiniones de nuestros alumnos en el Perú
              </Typography>
            </Box>
          </Grid>

          <Grid item xs={12} sm={6} lg={4} data-aos="zoom-in-up">
            <Card className={classes.root} align="center">
              <CardContent className={classes.content}>
                <FormatQuoteIcon className={classes.sizeIcon}  color="primary" fontSize="large"/>
                <Typography variant="p" component="p" >
                  "Profesionalizarme en el idioma inglés ha sido una linda experiencia. Las clases que dicta Christian son inolvidables por la gran calidad técnica de la plataforma que usa y  todas las herramientas digitales. Es simplemente una excelente manera de aprender".
                </Typography>
              </CardContent>
              <CardContent className={classes.content2}>
                <Typography className={classes.pos} color="primary" >
                Endrina Roa
                </Typography>
                <Typography variant="body2" component="p">
                National Key Account Manager Pharma
                </Typography>
                <Typography className={classes.pos} color="textSecondary" >
                Kuehne + Nagel 
                </Typography>
              </CardContent>
            </Card>
          </Grid>

          <Grid item xs={12} sm={6} lg={4} data-aos="zoom-in-up">
            <Card className={classes.root} align="center">
              <CardContent className={classes.content}>
                <FormatQuoteIcon className={classes.sizeIcon} color="primary" fontSize="large"/>
                <Typography variant="p" component="p" >
                "¡El mejor curso de inglés que he tomado! Las clases particulares online son increíbles ya que el profesor tiene una fluidez al hablar de nivel nativo. Estoy convencida que tomar el curso con él fue la mejor decision y que valió la pena la inversion."  
                </Typography>
              </CardContent>
              <CardContent className={classes.content2}>
                <Typography className={classes.pos} color="primary" >
                Lizeth Castro 
                </Typography>
                <Typography variant="body2" component="p">
                Account Manager Oncology
                </Typography>
                <Typography className={classes.pos} color="textSecondary" >
                Merck Sharp & Dohme
                </Typography>
              </CardContent>
            </Card>
          </Grid>

          <Grid  item xs={12} sm={6} lg={4} data-aos="zoom-in-up" >
            <Card className={classes.root} align="center">
              <CardContent className={classes.content}>
                <FormatQuoteIcon className={classes.sizeIcon} color="primary" fontSize="large"/>
                <Typography variant="p" component="p" >
                "Tomé varios cursos en distintos institutos y ninguno me gusto tanto como el del profesor Christian. Las clases personalizadas que llevamos mejoraron mi pronunciación y me permitieron llevar con éxito el semestre de intercambio estudiantil que tome en Europa."  
                </Typography>
              </CardContent>
              <CardContent className={classes.content2}>
                <Typography className={classes.pos} color="primary" >
                Alexandra Sarmiento
                </Typography>
                <Typography variant="body2" component="p">
                Estudiante de Administración de Empresas
                </Typography>
                <Typography className={classes.pos} color="textSecondary" >
                Universidad de Piura 
                </Typography>
              </CardContent>
            </Card>
          </Grid>

          
          </Grid>

        </Grid>
        </Grid>
        </div>
      </div>
    </div>
  );
}

QuintaSection.propTypes = {
  width: PropTypes.string.isRequired
};


export default withStyles(styles, { withTheme: true })(
  withWidth()(QuintaSection)
);
