import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Grid, Typography, withWidth, withStyles, Button, Box } from "@material-ui/core";

import calculateSpacing from "./calculateSpacing";
import imageHome2 from "../../dummy_data/images/imageHome2.jpg";
import SwipeableTextMobileStepper from "../../../shared/components/Carousel"

const styles = theme => ({

  image: {
    maxWidth: "50%",
    verticalAlign: "middle",
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.shadows[4]
  },
  Love: {
    fontFamily: "'Love Ya Like A Sister', cursive",
    fontWeight: 400
  },
});

function SegundaSection(props) {
  const { width, classes} = props;
  return (
   
      <div className="container-fluid lg-p-top lg-p-bottom">
        <div className={classNames("container-fluid", classes.container)}>
     

        <Grid container spacing={calculateSpacing(width)} >

          <Grid item xs={12} sm={12} md={12} className={classes.cardWrapper} 
       
          >
            <Grid
              container
              spacing={calculateSpacing(width)}
             
            >
              <Grid item xs={12} sm={12} lg={8}
              data-aos="zoom-in-up"
              data-aos-delay="0"
              align="center"
             
            >
            <Box pb={5}>
              <Typography variant="h4" align="center" className={classNames(classes.mb0)}>
              ¿POR QUÉ ES TAN EFECTIVO EL “YARANGA COACHING”?
              </Typography>
            </Box>

            <Box pb={5}>
              <Typography component="p" align="justify" className={classNames(classes.mb0)}>
              YARANGA COACHING se aproxima al aprendizaje desde sus bases, donde el lenguaje es una acción que se realiza. De esa manera, <Box display="inline" fontWeight="fontWeightBold">APRENDES HACIENDO</Box>, lo que significa que retienes el 90%. Pero hay más…
              </Typography>
            </Box>
              
            <Box pb={5}>
              <Typography component="p" align="justify" className={classNames(classes.mb0)}>
              Se ha demostrado que los humanos funcionamos mejor cuando sabemos que hay alguien que espera más de nosotros que nosotros mismos. Trabajamos aún más cuando queremos evitar decepcionar a alguien.
              </Typography>
            </Box>
              
            <Box pb={5}>
              <Typography component="p" align="justify" className={classNames(classes.mb0)}>
              YARANGA COACHING es tan efectivo porque estamos ahí para <Box display="inline" fontWeight="fontWeightBold">HACERTE RESPONSABLE</Box>, consistente y comprometido con el inglés.
              </Typography>

            </Box>
              
            <Box pb={5}>
              <Typography component="p" align="justify" className={classNames(classes.mb0)}>
              Aprender idiomas no debería hacerse sentado y con lápiz y papel. ¡Qué aburrido! YARANGA COACHING no funciona así. Lo único que necesitas es tu smartphone, tablet o laptop y una mente abierta.
              </Typography>
            </Box>
            
            <Box pb={5}>
              <img className={classes.image} src={imageHome2}></img>
            </Box>

            <Box pb={5}>
              <Typography variant="h6" align="center" className={classNames(classes.mb0)}>
              ¡Aprovecha de las videoclases online en vivo y diviértete!
              </Typography>
            </Box>
            </Grid>
             
            </Grid>
          
            
          </Grid>
          
        </Grid>


      
        </div>
        
      </div>
     
  );
}

SegundaSection.propTypes = {
  width: PropTypes.string.isRequired
};


export default withStyles(styles, { withTheme: true })(
  withWidth()(SegundaSection)
);
