import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Grid, Typography, withWidth, withStyles, Avatar, Box } from "@material-ui/core";

import calculateSpacing from "./calculateSpacing";
import imageHome3 from "../../dummy_data/images/imageHome3.jpg";
import IconCalendarImage from "../../dummy_data/images/icon-calendar.svg";
import IconActividadesImage from "../../dummy_data/images/icon-actividades.svg";
import IconClasesImage from "../../dummy_data/images/icon-clases.svg";
import CustomizedTables from "../../../shared/components/Table";

const styles = theme => ({
  containerFix: {
    [theme.breakpoints.down("md")]: {
      paddingLeft: theme.spacing(6),
      paddingRight: theme.spacing(6)
    }
  },
  image: {
    maxWidth: "100%",
    verticalAlign: "middle",
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.shadows[4]
  },
  Love: {
    fontFamily: "'Love Ya Like A Sister', cursive",
    fontWeight: 400
  },
  bgColor: {
    backgroundColor: "#F7F5F0",
  },
  large: {
    width: theme.spacing(12),
    height: theme.spacing(12),
  },
  mb0:{
    marginBottom: theme.spacing(0),
  }
});

function TerceraSection(props) {
  const { width, classes} = props;
  return (
    <div style={{ backgroundColor: "#F7F5F0" }}>
      <div className="container-fluid lg-p-top lg-p-bottom">
        
        <div className={classNames("container-fluid", classes.container)}>
        <Grid
          container
          spacing={calculateSpacing(width)}
        >
          <Grid item xs={12} sm={12} md={8} className={classes.cardWrapper} 
       
          >

        <Grid
          container
          spacing={calculateSpacing(width)}
        >
          
          <Grid item xs={12} sm={12} lg={12}
            
            data-aos="zoom-in-up"
            data-aos-delay="200"
          >
              <Grid
                container
                spacing={calculateSpacing(width)}
                className={classes.gridContainer}
              >
                <Grid
                  item
                  xs={12}
                  sm={12}
                  lg={12}
                  
                  data-aos="zoom-in-up"
                >
                  <Box pb={5}>
                    <Typography variant="h4" align="center" className={classNames(classes.mb0)}>
                    Descubre las ventajas exclusivas de YARANGA COACHING
                    </Typography>
                  </Box>
                  
                </Grid>


                <CustomizedTables />


              </Grid>
            
          </Grid>

        </Grid>

        </Grid>
        </Grid>
        </div>
      </div>
    </div>
  );
}

TerceraSection.propTypes = {
  width: PropTypes.string.isRequired
};


export default withStyles(styles, { withTheme: true })(
  withWidth()(TerceraSection)
);
