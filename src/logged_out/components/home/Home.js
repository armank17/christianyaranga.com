import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import HeadSection from "./HeadSection";
import SegundaSection from "./SegundaSection";
import TerceraSection from "./TerceraSection";
import CuartaSection from "./CuartaSection";
import QuintaSection from "./QuintaSection"

function Home(props) {
  const { selectHome } = props;
  useEffect(() => {
    selectHome();
  }, [selectHome]);
  return (
    <Fragment>
      <HeadSection />
      <SegundaSection />
      <TerceraSection />
      <CuartaSection />
      <QuintaSection />
    </Fragment>
  );
}

Home.propTypes = {
  selectHome: PropTypes.func.isRequired
};

export default Home;
