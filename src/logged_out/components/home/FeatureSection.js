import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Grid, Typography, isWidthUp, withWidth, withStyles } from "@material-ui/core";
import CodeIcon from "@material-ui/icons/Code";
import BuildIcon from "@material-ui/icons/Build";
import ComputerIcon from "@material-ui/icons/Computer";
import BarChartIcon from "@material-ui/icons/BarChart";
import HeadsetMicIcon from "@material-ui/icons/HeadsetMic";
import CalendarTodayIcon from "@material-ui/icons/CalendarToday";
import CloudIcon from "@material-ui/icons/Cloud";
import MeassageIcon from "@material-ui/icons/Message";
import CancelIcon from "@material-ui/icons/Cancel";
import calculateSpacing from "./calculateSpacing";
import FeatureCard from "./FeatureCard";

const styles = theme => ({
  Love: {
    fontFamily: "'Love Ya Like A Sister', cursive",
    fontWeight: 400
  },
});
const iconSize = 30;

const features = [
  {
    color: "#00C853",
    headline: "Funcional",
    text:
      "Las funciones del software son aquellas que buscan satisfacer las necesidades del usuario.",
    icon: <CalendarTodayIcon style={{ fontSize: iconSize }} />,
    mdDelay: "0",
    smDelay: "0"
  },
  {
    color: "#6200EA",
    headline: "Mantenible y Escalable",
    text:
      "Es decir, buenas prácticas que pueden ayudar a escribir un mejor código: más limpio, mantenible y escalable.",
    icon: <BuildIcon style={{ fontSize: iconSize }} />,
    mdDelay: "200",
    smDelay: "200"
  },
  {
    color: "#0091EA",
    headline: "Veloz",
    text:
      "A los usuarios no les gusta esperar para poder ver una página y sobre todo si hablamos de dispositivos móviles. Al tener una página rápida de cargar, evitas el aumento del porcentaje de abandono en tu sitio web.",
    icon: <MeassageIcon style={{ fontSize: iconSize }} />,
    mdDelay: "400",
    smDelay: "0"
  },
  {
    color: "#d50000",
    headline: "Responsivo y moderno",
    text:
      "El diseño responsivo es un diseño que responde al tamaño del dispositivo desde el que se está visualizando la web, adaptando las dimensiones del contenido y mostrando los elementos de una forma ordenada y optimizada sea cual sea el soporte.",
    icon: <ComputerIcon style={{ fontSize: iconSize }} />,
    mdDelay: "0",
    smDelay: "200"
  },
  {
    color: "#DD2C00",
    headline: "Eficiente",
    text:
      "Basada en la relación entre el nivel de rendimiento del software y el volumen de recursos utilizado, bajo ciertas condiciones.",
    icon: <BarChartIcon style={{ fontSize: iconSize }} />,
    mdDelay: "200",
    smDelay: "0"
  },
  {
    color: "#64DD17",
    headline: "Confiable",
    text:
      "La capacidad del software de mantener su rendimiento bajo ciertas condiciones durante cierto período de tiempo.",
    icon: <HeadsetMicIcon style={{ fontSize: iconSize }} />,
    mdDelay: "400",
    smDelay: "200"
  },
  {
    color: "#304FFE",
    headline: "Portable",
    text:
      "Basada en la capacidad del software para ser transferido de un entorno a otro.",
    icon: <CloudIcon style={{ fontSize: iconSize }} />,
    mdDelay: "0",
    smDelay: "0"
  },
  {
    color: "#C51162",
    headline: "Usable",
    text:
      "Basada en el esfuerzo necesario para utilizar el software por parte de un grupo de usuarios.",
    icon: <CodeIcon style={{ fontSize: iconSize }} />,
    mdDelay: "200",
    smDelay: "200"
  },
  {
    color: "#00B8D4",
    headline: "Compatible",
    text:
      "La compatibilidad es la condición que hace que un programa y un sistema, arquitectura o aplicación logren comprenderse correctamente tanto directamente o indirectamente (mediante un algoritmo).",
    icon: <CancelIcon style={{ fontSize: iconSize }} />,
    mdDelay: "400",
    smDelay: "0"
  }
];

function FeatureSection(props) {
  const { width, classes } = props;
  return (
    <div id="feature" style={{ backgroundColor: "#FFFFFF" }}>
      <div className="container-fluid lg-p-top">
        <Typography variant="h3" align="center" className={classNames(classes.Love, "lg-mg-bottom")}>
          Calidad de nuestro producto
        </Typography>
        <div className="container-fluid">
          <Grid container spacing={calculateSpacing(width)}>
            {features.map(element => (
              <Grid
                item
                xs={12}
                md={4}
                data-aos="zoom-in-up"
                data-aos-delay={
                  isWidthUp("md", width) ? element.mdDelay : element.smDelay
                }
                key={element.headline}
              >
                <FeatureCard
                  Icon={element.icon}
                  color={element.color}
                  headline={element.headline}
                  text={element.text}
                />
              </Grid>
            ))}
          </Grid>
        </div>
      </div>
    </div>
  );
}

FeatureSection.propTypes = {
  width: PropTypes.string.isRequired
};

export default withStyles(styles, { withTheme: true })(
  withWidth()(FeatureSection)
);

