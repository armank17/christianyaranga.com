import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Grid, Typography, withWidth, isWidthUp, withStyles, Box, Card, CardContent, CardActionArea, CardMedia, CardActions, Button } from "@material-ui/core";

import calculateSpacing from "./calculateSpacing";
import nocostosImage from "../../dummy_data/images/1584389386243.png";
import horarioImage from "../../dummy_data/images/1584389393282.png";
import aprenderImage from "../../dummy_data/images/1584389446528.png";
import FormatQuoteIcon from '@material-ui/icons/FormatQuote';

const styles = theme => ({
  containerFix: {
    [theme.breakpoints.down("md")]: {
      paddingLeft: theme.spacing(6),
      paddingRight: theme.spacing(6)
    }
  },
  image: {
    maxWidth: "100%",
    verticalAlign: "middle",
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.shadows[4]
  },
  Love: {
    fontFamily: "'Love Ya Like A Sister', cursive",
    fontWeight: 400
  },
  bgColor: {
    backgroundColor: "#F7F5F0",
  },
  white: {
    color: "#FFFFFF",
  },
  media: {
    height: "200px",
    backgroundColor: theme.palette.primary.main,
    padding: "40px 0px",
  },
  content: {
    height: "230px",
    //paddingBottom: "40px",
  }
});

function CuartaSection(props) {
  const { width, classes} = props;
  return (
    <div style={{ backgroundColor: "#000000" }}>
      <div className="container-fluid lg-p-top lg-p-bottom">
        
        <div className={classNames("container-fluid", classes.container)}>
        <Grid
          container
          spacing={calculateSpacing(width)}
        >
          <Grid item xs={12} sm={12} md={8} className={classes.cardWrapper} >
            <Grid
              container
              spacing={calculateSpacing(width)}
              className={classes.gridContainer}
            >
            
              
              <Grid item xs={12} sm={6} lg={4}
                data-aos="zoom-in-up"
              >
                <Card className={classes.root}>
                  <CardActionArea>
                    
                    <CardMedia
                      className={classes.media}
                      align="center"
                      title="Contemplative Reptile"
                    >
                      <img src={nocostosImage} />
                    </CardMedia>
                    <CardContent className={classes.content}>
                      <Box pb={3}>
                        <Typography gutterBottom variant="h5" component="h2">
                          No hay costo por materiales
                        </Typography>
                      </Box>
                      <Typography variant="body2" color="textSecondary" component="p">
                        Porque el curso online es completo y tiene todo lo que necesitas para aprender inglés.
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                  <CardActions>
                    
                  </CardActions>
                </Card>
              </Grid>

              <Grid item xs={12} sm={6} lg={4}
                data-aos="zoom-in-up"
              >
                <Card className={classes.root}>
                  <CardActionArea>
                    
                    <CardMedia
                      className={classes.media}
                      align="center"
                     
                      title="Contemplative Reptile"
                    >
                      <img src={horarioImage} />
                    </CardMedia>
                    <CardContent className={classes.content}>
                      <Box pb={3}>
                        <Typography gutterBottom variant="h5" component="h2">
                          El curso se ajusta a tu disponibilidad
                        </Typography>
                      </Box>
                      <Typography variant="body2" color="textSecondary" component="p">
                        Esto debido a que tu puedes elegir el horario y frecuencia que mejor se acomoden a ti.
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                  <CardActions>
                    
                  </CardActions>
                </Card>
              </Grid>

              <Grid item xs={12} sm={6} lg={4}
                data-aos="zoom-in-up"
              >
                <Card className={classes.root}>
                  <CardActionArea>
                    
                    <CardMedia
                      className={classes.media}
                      align="center"
                     
                      title="Contemplative Reptile"
                    >
                      <img src={aprenderImage} />
                    </CardMedia>
                    <CardContent className={classes.content}>
                      <Box pb={3}>
                        <Typography gutterBottom variant="h5" component="h2">
                          Garantizamos tu aprendizaje
                        </Typography>
                      </Box>
                      <Typography variant="body2" color="textSecondary" component="p">
                        Dado que el curso es 100% personalizado, éste es diseñado a la medida y se adecua a tus requerimientos.
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                  <CardActions>
                   
                  </CardActions>
                </Card>
              </Grid>

            </Grid>

        </Grid>
        </Grid>
        </div>
      </div>
    </div>
  );
}

CuartaSection.propTypes = {
  width: PropTypes.string.isRequired
};


export default withStyles(styles, { withTheme: true })(
  withWidth()(CuartaSection)
);
