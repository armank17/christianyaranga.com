import React, { Fragment } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Link } from "react-router-dom";
import {
  Grid,
  Typography,
  Button,
  Box,
  withStyles,
  withWidth,
  isWidthUp,
  Hidden,
} from "@material-ui/core";
import ImageheaderSectionHome from "../../dummy_data/images/headerSectionHome.jpg";
import calculateSpacing from "./calculateSpacing";
import PlanesFormik from "../../../shared/components/planes-formik/PlanesFormik";

import CyImage from "../../dummy_data/images/cy-photo.png";

//import StickyBox from "react-sticky-box";
import Sticky from 'react-sticky-el';
//import { StickyContainer, Sticky } from 'react-sticky';
const styles = theme => ({
  extraLargeButtonLabel: {
    fontSize: theme.typography.body1.fontSize,
    [theme.breakpoints.up("sm")]: {
      fontSize: theme.typography.h6.fontSize
    }
  },
  extraLargeButton: {
    paddingTop: theme.spacing(1.5),
    paddingBottom: theme.spacing(1.5),
    [theme.breakpoints.up("xs")]: {
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1)
    },
    [theme.breakpoints.up("lg")]: {
      paddingTop: theme.spacing(2),
      paddingBottom: theme.spacing(2)
    }
  },
  wrapper: {
    position: "relative",
    backgroundColor: theme.palette.secondary.main,
    paddingBottom: theme.spacing(2)
  },
  image: {
    maxWidth: "100%",
    verticalAlign: "middle",
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.shadows[4]
  },
  container: {
    //marginTop: theme.spacing(6),
    //marginBottom: theme.spacing(12),
    [theme.breakpoints.down("md")]: {
      marginBottom: theme.spacing(9)
    },
    [theme.breakpoints.down("sm")]: {
      marginBottom: theme.spacing(6)
    },
    [theme.breakpoints.down("sm")]: {
      marginBottom: theme.spacing(3)
    }
  },
  containerFix: {
    [theme.breakpoints.up("md")]: {
      maxWidth: "none !important"
    }
  },
  waveBorder: {
    paddingTop: theme.spacing(4)
  },
  Love: {
    fontFamily: "'Love Ya Like A Sister', cursive",
    fontWeight: 400,
    
  },
  bgBanner: {
    background: `url( ${ImageheaderSectionHome} )`,
    backgroundPosition: "50% 0",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    bottom: "auto",
    [theme.breakpoints.down("md")]: {
      background: 'inherit',
      backgroundColor: "#353535",
    },
  },
  colorWhite: {
    color: "#FFF",
  },
  image: {
    maxWidth: "70%",
    verticalAlign: "middle",
    //borderRadius: theme.shape.borderRadius,
    //boxShadow: theme.shadows[4]
    
  },
  gridForm: {
    [theme.breakpoints.down("md")]: {
     paddingTop: "0px!important",
    },
  }
});

function HeadSection(props) {
  const { classes, width } = props;
  return (
   
      <div  className={classNames(  classes.bgBanner)}>
        
        <div className={classNames("container-fluid", "lg-p-bottom",classes.container)}>
          <div className={classNames("container-fluid")}>
            <Grid
              container
              spacing={calculateSpacing(width)}
              className={classNames('',classes.gridContainer)}
            >
              <Grid item xs={12} sm={12} md={8}  
                data-aos-delay="0"
                data-aos="zoom-in"
              >
                <Box
                  display="flex"
                  flexDirection="column"
                  justifyContent="space-between"
                  height="100%"
                >
                  
                  <Box mb={4} 
                    mt={5}
                    display="flex"
                    //flexDirection="column"
                    alignItems="end"
                    height="70%"
                    width={isWidthUp("lg", width) ?"70%" : "100%"}
                  >
                    <Typography
                      variant={isWidthUp("lg", width) ? "h2" : "h2"}    
                      className={classes.colorWhite}  
                      align={isWidthUp("lg", width) ?"left" : "center"}              
                    >
                    Clases de inglés personalizadas 100% online
                    </Typography>
                  </Box>
                  <Box mb={4} 
                    display="flex"
                    //flexDirection="column"
                    alignItems="initial"
                    justifyContent={isWidthUp("lg", width) ?"left" : "center"} 
                    height={isWidthUp("lg", width) ?"30%" : "100%"}
                    
                  >
                    <Link                
                        to={'/como-aprender-ingles'}      
                        //target="_blank"                   
                      >
                        <Button
                          variant="contained"
                          color="primary"
                          
                          className={classes.extraLargeButtonLabel}
                          classes={{ label: classes.extraLargeButtonLabel }}
                          //href="https://api.whatsapp.com/send?phone=51967399473&text=Hola,%20estoy%20interesado%20en%20un%20p%C3%A1gina%20web."
                          //target="_blank"
                        >
                          Cómo funciona
                        </Button>
                      </Link>
                  </Box>
                  
                </Box>
              </Grid>

              <Hidden mdUp>
                <Grid xs={12} sm={12} md={4}  align="center">
                <img className={classNames(  classes.image)} src={CyImage} />
                </Grid>
                
              </Hidden>
              
              <Grid item xs={12} sm={12} md={4} className={classNames(  classes.gridForm)}>
                <Sticky disabled={isWidthUp("lg", width) ? false : true} topOffset={0} stickyStyle={{top: '100px'}}>      
                  <Grid xs={12} sm={12} md={12}>
                    <PlanesFormik />
                  </Grid>
                </Sticky>
              </Grid>
            </Grid>
          </div>
        </div>
      </div>
  );
}

HeadSection.propTypes = {
  classes: PropTypes.object,
  width: PropTypes.string,
  theme: PropTypes.object
};

export default withWidth()(
  withStyles(styles, { withTheme: true })(HeadSection)
);
