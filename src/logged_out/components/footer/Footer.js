import React from "react";
import PropTypes from "prop-types";
import {
  Grid,
  Typography,
  Box,
  IconButton,
  withStyles,
  withWidth,
  isWidthUp,
} from "@material-ui/core";
import Facebook from "@material-ui/icons/Facebook";
import WhatsApp from "@material-ui/icons/WhatsApp";
import transitions from "@material-ui/core/styles/transitions";
import FloatingButtonMenu from  '../../../shared/components/FloatingButtonMenu';
import LogoImage from "../../dummy_data/images/logo_v1.png";

const styles = theme => ({
  footerInner: {
    backgroundColor: "#000000",
    paddingTop: theme.spacing(8),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingBottom: theme.spacing(6),
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(10),
      paddingLeft: theme.spacing(16),
      paddingRight: theme.spacing(16),
      paddingBottom: theme.spacing(10)
    },
    [theme.breakpoints.up("md")]: {
      paddingTop: theme.spacing(10),
      paddingLeft: theme.spacing(10),
      paddingRight: theme.spacing(10),
      paddingBottom: theme.spacing(10)
    }
  },
  brandText: {
    fontFamily: "'Baloo Bhaijaan', cursive",
    fontWeight: 400,
    color: theme.palette.common.white
  },
  footerLinks: {
    marginTop: theme.spacing(2.5),
    marginBot: theme.spacing(1.5),
    color: theme.palette.common.white
  },
  infoIcon: {
    color: `${theme.palette.common.white} !important`,
    backgroundColor: "#33383b !important"
  },
  socialIcon: {
    fill: theme.palette.common.white,
    backgroundColor: "#33383b",
    borderRadius: theme.shape.borderRadius,
    "&:hover": {
      backgroundColor: theme.palette.primary.light
    }
  },
  link: {
    cursor: "Pointer",
    color: theme.palette.common.white,
    transition: transitions.create(["color"], {
      duration: theme.transitions.duration.shortest,
      easing: theme.transitions.easing.easeIn
    }),
    "&:hover": {
      color: theme.palette.primary.light
    }
  },
  whiteBg: {
    backgroundColor: theme.palette.common.white
  },
  noDecoration: {
    textDecoration: "none !important"
  }
});

const infos = [
  {
    icon: <WhatsApp />,
    description: "+51 920 011 775",
    link : "https://api.whatsapp.com/send?phone=+51920011775&text=Hola,%20estoy%20interesado%20en%20tu%20servicio,%20quisiera%20m%C3%A1s%20informaci%C3%B3n."
  },
  {
    icon: <Facebook />,
    description: "Facebook",
    link : "https://www.facebook.com/"
  }
];


function Footer(props) {
  const { classes, width } = props;
  return (
    <footer id="contactenos" className="">
      <FloatingButtonMenu />
      <div className={classes.footerInner}>
        <Grid container spacing={isWidthUp("md", width) ? 10 : 5}>
          
          
            
          
          <Grid item xs={12} md={12} lg={12} >
            <Box display="flex" justifyContent="center">
              <Typography variant="h6" paragraph className="text-white">
              Lecciones interactivas en línea
              </Typography>
            </Box>  
            <Box display="flex" pb={3} justifyContent="center">
            <img
              src={LogoImage}
              className={classes.image}
              alt="header example"
            />
            </Box>
            <Box display="flex" justifyContent="center">
              <Typography variant="h7" paragraph className="text-white">
              Tu escuela de inglés
              </Typography>
            </Box>  
            
          </Grid>
          <Grid item xs={12} md={12} lg={12} >
            
            <Box borderTop={1} pt={3} borderColor="primary.main" display="flex" justifyContent="left">
              <Typography variant="h7" style={{ color: "#8f9296" }} paragraph>
              
              Copyright © 2020 - All rights reserved.

              </Typography>
            </Box>
              
          </Grid>
        </Grid>
      </div>
    </footer>
  );
}

Footer.propTypes = {
  theme: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired
};

export default withWidth()(withStyles(styles, { withTheme: true })(Footer));
