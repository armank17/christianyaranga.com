import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Grid, Typography, withWidth, withStyles, Avatar, Box } from "@material-ui/core";

import calculateSpacing from "../home/calculateSpacing";

import imageHome3 from "../../dummy_data/images/Triangulo.jpg";
import IconTargetImage from "../../dummy_data/images/icon-target.svg";
import IconDocImage from "../../dummy_data/images/icon-doc.svg";

const styles = theme => ({
  containerFix: {
    [theme.breakpoints.down("md")]: {
      paddingLeft: theme.spacing(6),
      paddingRight: theme.spacing(6)
    }
  },
  image: {
    maxWidth: "100%",
    verticalAlign: "middle",
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.shadows[4]
  },
  Love: {
    fontFamily: "'Love Ya Like A Sister', cursive",
    fontWeight: 400
  },
  bgColor: {
    backgroundColor: "#F7F5F0",
  },
  large: {
    width: theme.spacing(12),
    height: theme.spacing(12),
  },
  mb0:{
    marginBottom: theme.spacing(0),
  }
});

function CDI_segundaSection(props) {
  const { width, classes} = props;
  return (
    <div style={{ backgroundColor: "#F7F5F0" }}>
      <div className="container-fluid lg-p-top lg-p-bottom">
        
        <div className={classNames("container-fluid", classes.container)}>
        <Grid
          container
          spacing={calculateSpacing(width)}
          className={classes.gridContainer}
        >
          <Grid
            item
            xs={12}
            sm={12}
            lg={12}
            className={classes.cardWrapper}
            data-aos="zoom-in-up"
            
          >
              <Grid
                container
                spacing={calculateSpacing(width)}
                className={classes.gridContainer}
              >
                <Grid
                  item
                  xs={12}
                  sm={12}
                  lg={12}
                  className={classes.cardWrapper}
                  data-aos="zoom-in-up"
                >
                 
                  <Typography variant="h4" className={classNames(classes.mb0)}>
                  IELTS
                  </Typography>
                  <Box mt={5} mb={5}> 
                    <Typography paragraph color="body">
                    El International English Language Testing System (IELTS) es un test estandarizado de inglés utilizado principalmente por los estudiantes que quieren acceder a universidades en el extranjero y por personas que desean emigrar al Reino Unido u otros países de la Commonwealth. Hay tres versiones del IELTS, el IELTS General, el IELTS Académico y el IELTS Life Skills. El IELTS Académico es el más popular con diferencia, representa aproximadamente el 80 % de los test IELTS realizados cada año. Dicho esto, si necesitas una determinada puntuación para tu visado o acceso a la universidad los cursos de preparación para el IELTS que tiene YARANGA COACHING te pueden ayudar a maximizar tu puntuación.
                    </Typography>
                  </Box>
                  
                  
                 
                </Grid>

                <Grid
                  item
                  xs={12}
                  sm={12}
                  lg={12}
                  className={classes.cardWrapper}
                  data-aos="zoom-in-up"
                >
                  <Box mt={5}>
                    <Typography variant="h4" className={classNames(classes.mb0)}>
                    TOEFL
                    </Typography>
                  </Box>
                  
                  <Box mt={5}> 
                    <Typography paragraph color="body">
                    El TOEFL (Test of English as a Foreign Language) es una prueba estandarizada de inglés académico realizada principalmente por estudiantes que desean acceder a universidades de los Estados Unidos. Algunas universidades de otros países también aceptan el TOEFL como prueba del nivel de inglés.  La puntuación exigida en el TOEFL varía según la universidad, y algunas también pueden exigir puntuaciones específicas en alguna de las destrezas evaluadas. Las puntuaciones del TOEFL  tienen una validez de 2 años desde la fecha de realización del test. YARANGA COACHING te ofrece un plan de estudios estructurado para que puedas alcanzar una alta puntuación en el TOEFL.
                    </Typography>
                  </Box>
                </Grid>


                <Grid
                  item
                  xs={12}
                  sm={12}
                  lg={12}
                  className={classes.cardWrapper}
                  data-aos="zoom-in-up"
                >
                  <Box mt={5}>
                    <Typography variant="h4" className={classNames(classes.mb0)}>
                    GMAT
                    </Typography>
                  </Box>
                  
                  <Box mt={5}> 
                    <Typography paragraph color="body">
                    El Graduate Management Admission Test (GMAT) es un examen estandarizado y adaptativo que mide la capacidad matemática y verbal del candidato para tener éxito académico en los estudios de posgrado en negocios (MBA). Dicho esto, si tu deseo es hacer una maestría en una universidad top del extranjero YARANGA COACHING te ofrece el curso de preparación más completo que comprende: composición analitica y razonamiento integrado tanto verbal como matemático.  
                    </Typography>
                  </Box>
                </Grid>
               

              </Grid>
            
          </Grid>
          
        </Grid>

        
        </div>
      </div>
    </div>
  );
}

CDI_segundaSection.propTypes = {
  width: PropTypes.string.isRequired
};


export default withStyles(styles, { withTheme: true })(
  withWidth()(CDI_segundaSection)
);
