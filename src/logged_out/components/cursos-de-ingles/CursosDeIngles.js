import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import CaiBannerSection from "./CAI_BannerSection";
import CdiSegundaSection from "./CDI_segundaSection";

function CursosDeIngles(props) {
  const { selectCursosDeIngles } = props;
  useEffect(() => {
    selectCursosDeIngles();
  }, [selectCursosDeIngles]);
  return (
    <Fragment>
      <CaiBannerSection />
      <CdiSegundaSection />
    </Fragment>
  );
}

CursosDeIngles.propTypes = {
  selectCursosDeIngles: PropTypes.func.isRequired
};

export default CursosDeIngles;
