import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Grid, Typography, isWidthUp, withWidth, withStyles } from "@material-ui/core";

import calculateSpacing from "../home/calculateSpacing";
import NosotrosImage from "../../dummy_data/images/nosotros2.jpg";
import ContactForm from "../../../shared/components/ContactForm";
import ContactFormik from "../../../shared/components/ContactFormik";

const styles = theme => ({
  containerFix: {
    [theme.breakpoints.down("md")]: {
      paddingLeft: theme.spacing(6),
      paddingRight: theme.spacing(6),
    }
  },
  image: {
    maxWidth: "100%",
    verticalAlign: "middle",
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.shadows[4],
  },
  Love: {
    fontFamily: "'Love Ya Like A Sister', cursive",
    fontWeight: 400
  },
  spacing: theme.spacing(2),
});

function ContactenosSection(props) {
  const { width, classes} = props;
  return (
    <div id="nosotros" style={{ backgroundColor: "#FFFFFF" }}>
      <div className="container-fluid lg-p-top">
        <Typography 
          variant="h3" 
          align="center" 
          className={classNames(classes.Love, "lg-mg-bottom")} 
        >
          Contáctenos
        </Typography>
        <div className={classNames("container-fluid", classes.containerFix)}>
        <Grid
          container
          spacing={calculateSpacing(width)}
          className={classes.gridContainer}
        >
          <Grid
            item
            xs={12}
            sm={6}
            lg={6}
            className={classes.cardWrapper}
            data-aos="zoom-in-up"
          >
            <img
              src={NosotrosImage}
              className={classes.image}
              alt="header example"
            />
          </Grid>
          <Grid
            item
            xs={12}
            sm={6}
            lg={6}
            className={classes.cardWrapper}
            data-aos="zoom-in-up"
            data-aos-delay="200"
          >
              <Grid
                container
                spacing={calculateSpacing(width)}
                className={classes.gridContainer}
              >
                <Grid
                  item
                  xs={12}
                  sm={12}
                  lg={12}
                  className={classes.cardWrapper}
                  data-aos="zoom-in-up"
                >
                  
                 

                  <ContactFormik />
                </Grid>

                

                


              </Grid>
            
          </Grid>

        </Grid>


        
        </div>
      </div>
    </div>
  );
}

ContactenosSection.propTypes = {
  width: PropTypes.string.isRequired
};


export default withStyles(styles, { withTheme: true })(
  withWidth()(ContactenosSection)
);
