import React from "react";
import PropTypes from "prop-types";
import { Grid, Typography, isWidthUp, withWidth } from "@material-ui/core";


function ServiciosSection(props) {
  const { width } = props;
  return (
    <div style={{ backgroundColor: "#FFFFFF" }}>
      <div className="container-fluid lg-p-top">
        <Typography variant="h3" align="left" className="lg-mg-bottom">
          NUESTROS SERVICIOS
        </Typography>
        <div className="container-fluid">
          
        </div>
      </div>
    </div>
  );
}

ServiciosSection.propTypes = {
  width: PropTypes.string.isRequired
};

export default withWidth()(ServiciosSection);
