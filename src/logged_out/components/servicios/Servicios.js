import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import ServiciosSection from "./ServiciosSection";

function Servicios(props) {
  const { selectServicios } = props;
  useEffect(() => {
    selectServicios();
  }, [selectServicios]);
  return (
    <Fragment>
     <ServiciosSection />
    </Fragment>
  );
}

Servicios.propTypes = {
  selectServicios: PropTypes.func.isRequired
};

export default Servicios;
