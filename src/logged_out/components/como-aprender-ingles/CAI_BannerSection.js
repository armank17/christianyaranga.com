import React, { Fragment } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Grid, Typography, isWidthUp, withWidth, withStyles, Box, Card} from "@material-ui/core";

import ImageHeaderSectioncomofunciona from "../../dummy_data/images/headerSectioncomofunciona.jpg";
import PlanesFormik from "../../../shared/components/planes-formik/PlanesFormik";

const styles = theme => ({
  extraLargeButtonLabel: {
    fontSize: theme.typography.body1.fontSize,
    [theme.breakpoints.up("sm")]: {
      fontSize: theme.typography.h6.fontSize
    }
  },
  extraLargeButton: {
    paddingTop: theme.spacing(1.5),
    paddingBottom: theme.spacing(1.5),
    [theme.breakpoints.up("xs")]: {
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1)
    },
    [theme.breakpoints.up("lg")]: {
      paddingTop: theme.spacing(2),
      paddingBottom: theme.spacing(2)
    }
  },
  card: {
    boxShadow: theme.shadows[0],
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("xs")]: {
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(3)
    },
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(5),
      paddingBottom: theme.spacing(5),
      paddingLeft: theme.spacing(4),
      paddingRight: theme.spacing(4)
    },
    [theme.breakpoints.up("md")]: {
      paddingTop: theme.spacing(5.5),
      paddingBottom: theme.spacing(5.5),
      paddingLeft: theme.spacing(5),
      paddingRight: theme.spacing(5)
    },
    [theme.breakpoints.up("lg")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
      paddingLeft: theme.spacing(6),
      paddingRight: theme.spacing(6)
    },
    [theme.breakpoints.down("lg")]: {
      width: "auto"
    },
    backgroundColor: "inherit"
  },
  wrapper: {
    position: "relative",
    backgroundColor: theme.palette.secondary.main,
    paddingBottom: theme.spacing(2)
  },
  image: {
    maxWidth: "100%",
    verticalAlign: "middle",
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.shadows[4]
  },
  container: {
    //marginTop: theme.spacing(6),
    //marginBottom: theme.spacing(12),
    [theme.breakpoints.down("md")]: {
      marginBottom: theme.spacing(9)
    },
    [theme.breakpoints.down("sm")]: {
      marginBottom: theme.spacing(6)
    },
    [theme.breakpoints.down("sm")]: {
      marginBottom: theme.spacing(3)
    }
  },
  containerFix: {
    [theme.breakpoints.up("md")]: {
      maxWidth: "none !important"
    }
  },
  waveBorder: {
    paddingTop: theme.spacing(4)
  },
  Love: {
    fontFamily: "'Love Ya Like A Sister', cursive",
    fontWeight: 400,
    
  },
  bgBanner: {
    background: `url( ${ImageHeaderSectioncomofunciona} )`,
    backgroundPosition: "50% 0",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    bottom: "auto",
  },
  colorWhite: {
    color: "#FFF",
    textShadow: "0px 1px 2px rgba(0, 0, 0, 0.5)"
  }
});

function CAI_BannerSection(props) {
  const { width, classes} = props;
  return (
    <Fragment>
      <div className={classNames( classes.wrapper, classes.bgBanner)}>
        <div className={classNames("container-fluid", classes.container)}>
          <Box display="flex" justifyContent="center" className="row">
            <Card
              className={classes.card}
              data-aos-delay="200"
              data-aos="zoom-in"
            >
              <div className={classNames(classes.containerFix, "container")}>
                <Box justifyContent="space-between" className="row">
                  <Grid item xs={12} md={12}>
                  <Box display="flex" justifyContent="center" className="row">
                  <Grid item xs={12} md={12} lg={8}>
                        <Typography
                          variant={isWidthUp("lg", width) ? "h3" : "h3"}    
                          className={classes.colorWhite}
                          align="center"
                          //color="black"               
                        >
                          EL MÉTODO DE YARANGA COACHING
                        </Typography>
                        
                  </Grid>
                  <Grid item xs={12} md={12} lg={9}>
                    <Typography 
                      variant="h5"
                      paragraph
                      className={classNames(classes.colorWhite, 'mt-5')}
                      align="center"
                    >
                    LAS APPS Y LAS CLASES CONVENCIONALES NO FUNCIONAN PARA APRENDER IDIOMAS ¿POR QUÉ?
                    </Typography>
                  </Grid>
                  
                 
                  
                </Box>
                    
                  </Grid>
                  
                 
                  
                </Box>
              </div>
            </Card>
          </Box>
        </div>
      </div>
      
    </Fragment>
  );
}

CAI_BannerSection.propTypes = {
  width: PropTypes.string.isRequired
};


export default withStyles(styles, { withTheme: true })(
  withWidth()(CAI_BannerSection)
);
