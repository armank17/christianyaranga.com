import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import CaiBannerSection from "./CAI_BannerSection";
import CaiSegundaSection from "./CAI_segundaSection";

function Nosotros(props) {
  const { selectComoAprenderIngles } = props;
  useEffect(() => {
    selectComoAprenderIngles();
  }, [selectComoAprenderIngles]);
  return (
    <Fragment>
      <CaiBannerSection />
      <CaiSegundaSection />
    </Fragment>
  );
}

Nosotros.propTypes = {
  selectComoAprenderIngles: PropTypes.func.isRequired
};

export default Nosotros;
