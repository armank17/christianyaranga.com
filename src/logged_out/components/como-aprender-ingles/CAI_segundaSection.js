import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Grid, Typography, withWidth, withStyles, Avatar, Box } from "@material-ui/core";

import calculateSpacing from "../home/calculateSpacing";

import imageHome3 from "../../dummy_data/images/Triangulo.jpg";
import IconTargetImage from "../../dummy_data/images/icon-target.svg";
import IconDocImage from "../../dummy_data/images/icon-doc.svg";

const styles = theme => ({
  containerFix: {
    [theme.breakpoints.down("md")]: {
      paddingLeft: theme.spacing(6),
      paddingRight: theme.spacing(6)
    }
  },
  image: {
    maxWidth: "100%",
    verticalAlign: "middle",
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.shadows[4]
  },
  Love: {
    fontFamily: "'Love Ya Like A Sister', cursive",
    fontWeight: 400
  },
  bgColor: {
    backgroundColor: "#F7F5F0",
  },
  large: {
    width: theme.spacing(12),
    height: theme.spacing(12),
  },
  mb0:{
    marginBottom: theme.spacing(0),
  }
});

function CAI_segundaSection(props) {
  const { width, classes} = props;
  return (
    <div style={{ backgroundColor: "#F7F5F0" }}>
      <div className="container-fluid lg-p-top lg-p-bottom">
        
        <div className={classNames("container-fluid", classes.container)}>
        <Grid
          container
          spacing={calculateSpacing(width)}
          className={classes.gridContainer}
        >
          <Grid
            item
            xs={12}
            sm={12}
            lg={4}
            className={classes.cardWrapper}
            data-aos="zoom-in-up"
            
          >
              <Grid
                container
                spacing={calculateSpacing(width)}
                className={classes.gridContainer}
              >
                <Grid
                  item
                  xs={12}
                  sm={12}
                  lg={12}
                  className={classes.cardWrapper}
                  data-aos="zoom-in-up"
                >
                  <Box mb={5}>
                    <Typography variant="h4" className={classNames(classes.mb0)}>
                    ¿POR QUÉ?
                    </Typography>
                  </Box>
                  <Typography paragraph color="body">
                  Porque son métodos pasivos en los que lees, escribes y escuchas, pero no HACES nada con el idioma que estás aprendiendo.
                  </Typography>
                  <Typography paragraph color="body">
                  Las apps, clases y cursos online se basan en el aprendizaje desde la <Box display="inline" fontWeight="fontWeightBold">parte superior</Box> de la pirámide. Esto significa que en sólo 2 semanas ya habrás olvidado la mitad de lo que aprendiste
                  </Typography>
                  <Typography paragraph color="body">
                  Échale un vistazo a la imagen de la pirámide
                  </Typography>
                  
                </Grid>

                
               

              </Grid>
            
          </Grid>
          <Grid
            item
            xs={12}
            sm={12}
            lg={8}
            className={classes.cardWrapper}
            data-aos="zoom-in-up"
            data-aos-delay="200"
          >
            <img
              src={imageHome3}
              className={classes.image}
              alt="header example"
            />
          </Grid>
        </Grid>

        
        </div>
      </div>
    </div>
  );
}

CAI_segundaSection.propTypes = {
  width: PropTypes.string.isRequired
};


export default withStyles(styles, { withTheme: true })(
  withWidth()(CAI_segundaSection)
);
