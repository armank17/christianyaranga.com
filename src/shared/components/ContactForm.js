import React from "react";
import classNames from "classnames";
import { withWidth, withStyles } from "@material-ui/core";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ReactFormInputValidation from "react-form-input-validation";
import axios from "axios";

const styles = theme => ({
    spacing: theme.spacing(2),
    Love: {
        ntFamily: "'Love Ya Like A Sister', cursive",
        fontWeight: 400,
    },
    containerFix: {
        [theme.breakpoints.down("md")]: {
            paddingLeft: theme.spacing(6),
            paddingRight: theme.spacing(6),
        }
    },
});

class ContactForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      fields: {
        name: "",
        email: "",
        phone_number: "",
        mensaje: ""
      },
      mailSent: false,
      error: null,
      errors: {}
    };

    this.form = new ReactFormInputValidation(this);

    this.form.useRules({
        name: "required",
        email: "required|email",
        phone_number: "required|numeric|digits_between:7,9",
        mensaje: "required|max:250",
    });

    this.form.onformsubmit = (fields) => {
        // Do you ajax calls here.
        //console.log(fields.email);

        //var form = new FormData();
        //form.append('email', 'oli');

        axios.post('http://api.armandoaliaga.top/send3.php', {
            data: fields,
        }) .then(function (result) {
           
        });

        /*axios({
            method: "post",
            url: "https://api.armandoaliaga.top/send2.php",
            headers: { "Content-Type": "application / json" },
            data: fields,
          })
            .then(result => {
              if (result.data.sent) {
                this.setState({
                  mailSent: result.data.sent
                });
                this.setState({ error: false });
              } else {
                this.setState({ error: true });
              }
            })
            .catch(error => this.setState({ error: error.message }));
        */
    }
  }
 
  render() {
    //console.log(process.env.REACT_APP_API);
    const { classes } = this.props;
    return (<React.Fragment>
        <form onSubmit={this.form.handleSubmit}>
            <div>
                <TextField 
                    id="standard-basic"
                    name="name"
                    fullWidth
                    label="Nombre"
                    helperText={this.state.errors.name ? this.state.errors.name : ""}
                    onBlur={this.form.handleBlurEvent}
                    error={this.state.errors.name ? true : false}
                    onChange={this.form.handleChangeEvent}
                    value={this.state.fields.name}
                    
                    //className={classes.gridContainer}
                />
            </div>
            <div className={classNames("mt-5")}>
                <TextField 
                    id="standard-basic"
                    name="email"
                    fullWidth
                    label="Correo"
                    helperText={this.state.errors.email ? this.state.errors.email : ""}
                    onBlur={this.form.handleBlurEvent}
                    error={this.state.errors.email ? true : false}
                    onChange={this.form.handleChangeEvent}
                    value={this.state.fields.email}
                />
            </div>
            <div className={classNames("mt-5")}>
                <TextField 
                    id="standard-basic"
                    name="phone_number"
                    fullWidth
                    label="Teléfono"
                    helperText={this.state.errors.phone_number ? this.state.errors.phone_number : ""}
                    onBlur={this.form.handleBlurEvent}
                    error={this.state.errors.phone_number ? true : false}
                    onChange={this.form.handleChangeEvent}
                    value={this.state.fields.phone_number}
                />
            </div>
            <div className={classNames("mt-5")}>
                <TextField
                    id="standard-multiline-static"
                    name="mensaje"
                    fullWidth
                    label="Mensaje"
                    multiline
                    rows={3}
                    helperText={this.state.errors.mensaje ? this.state.errors.mensaje : ""}
                    onBlur={this.form.handleBlurEvent}
                    error={this.state.errors.mensaje ? true : false}
                    onChange={this.form.handleChangeEvent}
                    value={this.state.fields.mensaje}
                />
            </div>           
            <div className={classNames("mt-5")}>
                <Button
                    type="submit"
                    variant="contained"  
                >
                    Enviar
                </Button>
            </div>
            <div className={classNames("mt-5")}>
              {this.state.mailSent && <div className="success">Exito</div>}
              {this.state.error && <div className="error">Error</div>}
            </div>

        </form>
    </React.Fragment>)
  }
}

export default withStyles(styles, { withTheme: true })(
    withWidth()(ContactForm)
);