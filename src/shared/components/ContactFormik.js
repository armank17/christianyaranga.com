import React from 'react';
import PropTypes from "prop-types";
import classNames from "classnames";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withWidth, withStyles } from "@material-ui/core";
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from "axios";

const styles = theme => ({
  otro:{
    marginRight: theme.spacing(2),
  }
});

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

function ContactFormik(props) {

  const { width, classes} = props;
  
  const formik = useFormik({
    initialValues: {
      name: '',
      email:     '',
      phone_number: '',
      mensaje:  '',
    },
    validationSchema: Yup.object({
      name: Yup.string()
        .max(15, 'Máximo 15 caracteres')
        .required('El nombre es olbigatorio : )'),
      email: Yup.string()
        .email('Correo electrónico invalido : (')
        .required('El correo es obligatorio : )'),
      phone_number: Yup.string()
        .matches(phoneRegExp, 'El teléfono no es valido')
        .max(10, 'Máximo 10 caracteres')
        .required('El teléfono es obligatorio'),
      mensaje: Yup.string()
        .max(200, 'Máximo 20 caracteres')
        .required('El mensaje es obligatorio : )'),
    }),
    onSubmit: values => {
      //
      axios.post('http://api.armandoaliaga.top/send3.php', {
        data: values,
      }).then(function (result) {
        //alert(JSON.stringify(values, null, 2));
        if (result.data.sent) {
          formik.handleReset();
        }else {
            
        }
      });
    },
    
  });
  return (
    <form onSubmit={formik.handleSubmit}>
      <div>
          <TextField 
              name="name"
              fullWidth
              label="Nombre"
              helperText={ formik.touched.name && formik.errors.name ? formik.errors.name : ""}
              error={ formik.errors.name ? true : false}
              type="text"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.name}  
          />
      </div>
      <div>
          <TextField 
              name="email"
              fullWidth
              label="Correo"
              helperText={ formik.touched.email && formik.errors.email ? formik.errors.email : ""}
              error={ formik.errors.email ? true : false}
              type="email"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.email}  
          />
      </div>
      <div>
          <TextField 
              name="phone_number"
              fullWidth
              label="Teléfono"
              helperText={ formik.touched.phone_number && formik.errors.phone_number ? formik.errors.phone_number : ""}
              error={ formik.errors.phone_number ? true : false}
              type="text"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.phone_number}  
          />
      </div>
      <div>
          <TextField 
              name="mensaje"
              fullWidth
              label="Mensaje"
              multiline
              rows={3}
              helperText={ formik.touched.mensaje && formik.errors.mensaje ? formik.errors.mensaje : ""}
              error={ formik.errors.phone_number ? true : false}
              type="text"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.mensaje}  
          />
      </div>         

      <div className={classNames("mt-5")}>
        <Button
          className={classNames(classes.otro)} 
          type="button"
          variant="contained"
          onClick={formik.handleReset}
          disabled={!formik.dirty || formik.isSubmitting}
        >
          Limpiar
        </Button>

        <Button
          type="submit"
          variant="contained"  
        >
          Enviar
        </Button>
      </div>

    </form>
  );
};

ContactFormik.propTypes = {
    width: PropTypes.string.isRequired
  };
  
  
export default withStyles(styles, { withTheme: true })(
  withWidth()(ContactFormik)
);