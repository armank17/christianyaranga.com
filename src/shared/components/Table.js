import React from 'react';
import { withWidth} from "@material-ui/core";
import classNames from "classnames";
import {  withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function createData(name, calories, fat) {
  return { name, calories, fat};
}

const rows = [
  createData('Clases en vivo 100% online', <CheckCircleIcon color="primary" />, <HighlightOffIcon />),
  createData('Grabación de cada clase dictada para que repases cuando gustes', <CheckCircleIcon color="primary" />, <HighlightOffIcon />),
  createData('Redacción  corregida por el profesor en tiempo real', <CheckCircleIcon color="primary" />, <HighlightOffIcon />),
  createData('Cursos especializados hechos a la medida (entrevistas de trabajo, exámenes internacionales, etc)', <CheckCircleIcon color="primary" />,  <CheckCircleIcon color="primary" />),
  createData('Clases exclusivas de inglés de negocios', <CheckCircleIcon color="primary" />,  <CheckCircleIcon color="primary" />),
  createData('Clases cuando y donde quieras solo con conexión a internet', <CheckCircleIcon color="primary"/>, <HighlightOffIcon />),
];

const useStyles = makeStyles({
  table: {
    //minWidth: 700,
    overflowX: 'inherit',
   
  },
  table2: {
    //minWidth: 700,
    overflowX: 'inherit',
    width: 'inherit',
  },
  th2: {
    minWidth: 100,
  },
});

const styles = theme => ({
  th: {
    minWidth: 100,
    [theme.breakpoints.down("md")]: {
       minWidth: 100,
    }
  },

});

function CustomizedTables() {
  const classes = useStyles();

  return (
    <TableContainer className={classes.table2} component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell> </StyledTableCell>
            <StyledTableCell align="center">YARANGA COACHING</StyledTableCell>
            <StyledTableCell align="center" className={classes.th2}>Otros cursos de inglés</StyledTableCell>
            
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.name}>
              <StyledTableCell component="th" scope="row">
                {row.name}
              </StyledTableCell>
              <StyledTableCell align="center">{row.calories}</StyledTableCell>
              <StyledTableCell align="center">{row.fat}</StyledTableCell>
              
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default withStyles(styles, { withTheme: true })(
  withWidth()(CustomizedTables)
);
