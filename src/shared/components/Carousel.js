import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';

import imageHome3 from "../../../src/logged_out/dummy_data/images/imageHome3.jpg";
import imageHome2 from "../../../src/logged_out/dummy_data/images/imageHome2.jpg";
import imageHome4 from "../../../src/logged_out/dummy_data/images/imageHome4.jpg";
import ImageHome5 from "../../../src/logged_out/dummy_data/images/ImageHome5.jpg";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const tutorialSteps = [
  {
    label: 'Actividades interactivas:',
    subLabel: 'Diseñadas a tu medida para que aprendas de forma práctica y efectiva.',
    imgPath:
        imageHome2,
  },
  {
    label: 'Cursos Especializados:',
    subLabel: 'Incrementa tus competencias a través de cursos especializados en Business, Hotelería, Viajes y mucho más',
    imgPath:
        imageHome3,
  },
  {
    label: 'Clases con Profesores:',
    subLabel: 'En grupos, cada 30 minutos y con temas que cambian diariamente, o particulares con temas personalizado y retroalimentación de los profesores expertos para avanzar en cada etapa del curso.',
    imgPath:
        imageHome4,
  },
  {
    label: 'Certificados:',
    subLabel: 'Cada vez que termines un nivel, haz un test y obtén un certificado para compartir en tu LinkedIn y demostrar tu nivel de fluidez',
    imgPath:
        ImageHome5,
  },
  
];

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 550,
    flexGrow: 1,
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    backgroundColor: theme.palette.background.default,
  },
  subHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: 130,
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    backgroundColor: theme.palette.background.default,
  },
  img: {
    height: 255,
    display: 'block',
    maxWidth: 550,
    overflow: 'hidden',
    width: '100%',
  },
}));

function SwipeableTextMobileStepper() {
  const classes = useStyles();
  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);
  const maxSteps = tutorialSteps.length;

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
    <div className={classes.root}>
      
      <AutoPlaySwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={activeStep}
        onChangeIndex={handleStepChange}
        enableMouseEvents
      >
        {tutorialSteps.map((step, index) => (
          <div key={step.label}>
            {Math.abs(activeStep - index) <= 2 ? (
              <img className={classes.img} src={step.imgPath} alt={step.label} />
            ) : null}
          </div>
        ))}
      </AutoPlaySwipeableViews>
      <Paper square elevation={0} className={classes.header} >
        <Typography variant="h5" align="center">{tutorialSteps[activeStep].label}</Typography>
      </Paper>
      <Paper square elevation={0} className={classes.subHeader}>
        <Typography paragraph>{tutorialSteps[activeStep].subLabel}</Typography>
      </Paper>
      <MobileStepper
        steps={maxSteps}
        position="static"
        variant="text"
        activeStep={activeStep}
        nextButton={
          <Button size="small" onClick={handleNext} disabled={activeStep === maxSteps - 1}>
            Next
            {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
          </Button>
        }
        backButton={
          <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
            {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            Back
          </Button>
        }
      />
    </div>
  );
}

export default SwipeableTextMobileStepper;
