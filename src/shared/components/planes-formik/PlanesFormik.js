import React, { Component } from 'react';
import PropTypes from "prop-types";
import classNames from "classnames";

import { Card, Typography, withWidth, withStyles, isWidthUp, Select, TextField, Button  } from "@material-ui/core";

import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import axios from "axios";
import { CountryRegionData } from 'react-country-region-selector';
import TrancisionAlert from './TrancisionAlert';


const styles = theme => ({
  otro:{
    marginRight: theme.spacing(2),
  },
  pt: {
    marginTop: theme.spacing(2)
  },
  card: {
    boxShadow: theme.shadows[5],
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("xs")]: {
      marginTop: theme.spacing(3),
      marginLeft: theme.spacing(0),
      marginRight: theme.spacing(0),
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(3),
      paddingLeft: theme.spacing(3),
      paddingRight: theme.spacing(3),
    },
    
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(5),
      paddingBottom: theme.spacing(5),
      paddingLeft: theme.spacing(4),
      paddingRight: theme.spacing(4)
    },
    [theme.breakpoints.up("md")]: {
      paddingTop: theme.spacing(5.5),
      paddingBottom: theme.spacing(5.5),
      paddingLeft: theme.spacing(5),
      paddingRight: theme.spacing(5)
    },
    [theme.breakpoints.up("lg")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
      paddingLeft: theme.spacing(6),
      paddingRight: theme.spacing(6)
    },
    [theme.breakpoints.down("lg")]: {
      width: "auto",
      marginTop: theme.spacing(0),
    },
    //backgroundColor: "inherit"
  },
});


const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
const ageRegExp = /([1-9][0-9])/
 
class ContactFormik extends Component{

  constructor (props) {
    super(props);
    this.state = { 
      submitForm: false,
    };
  }

  render() {
    const getRegions = country => {
      return new Promise((resolve, reject) => {
        resolve(CountryRegionData[country][2].split("|"));
      });
    };
    return(
      <Formik
        initialValues={{
          pais: 173,
          pais_name: "Peru",
          firstname: '',
          lastname: '',
          email:     '',
          departament:  "Lima",
          phone_number: '',
          age:  '',
          regions: CountryRegionData[173][2].split("|"),
        }}
        validationSchema={Yup.object().shape({
          pais: Yup.string()
            .max(15, 'Máximo 15 caracteres')
            .required('El país es olbigatorio'),
          firstname: Yup.string()
            .max(15, 'Máximo 15 caracteres')
            .required('El nombre es olbigatorio'),
          lastname: Yup.string()
            .max(15, 'Máximo 15 caracteres')
            .required('El apellido es olbigatorio'),
          email: Yup.string()
            .email('Correo electrónico invalido')
            .required('El correo es obligatorio'),
          departament: Yup.string()
            .max(15, 'Máximo 15 caracteres')
            .required('El departamento es olbigatorio'),
          phone_number: Yup.string()
            .matches(phoneRegExp, 'El teléfono no es valido')
            .max(10, 'Máximo 10 caracteres')
            .required('El teléfono es obligatorio'),
          age: Yup.string()
            .matches(ageRegExp, 'La edad no es valida')
            .max(2, 'Máximo 2 caracteres')
            .required('La edad es obligatorio'),
        })}
        onSubmit={(fields, {resetForm} ) => {
            //alert('SUCCESS!! :-)\n\n' + JSON.stringify(fields, null, 4));
            var self = this;
            axios.post('https://api.christianyaranga.com/sendmail.php', {
              data: fields,
            }).then(function (result) {
              //alert(JSON.stringify(values, null, 2));
              //alert(JSON.stringify(result));
              if (result.data.sent) {
                resetForm({values: ''})
                self.setState({ submitForm: true });
              }
            });
            
        }}
        render={({ values, errors, status, touched, handleChange, handleBlur, handleSubmit, handleReset, dirty, isSubmitting, setFieldValue }) => (
          <Card
            className={this.props.classes.card}
            
          >
          <Typography
            variant={isWidthUp("lg", this.props.width) ? "h6" : "h6"}
            align="center"                
          >
          Conoce nuestros planes y precios
          </Typography>
          <Form>
            <div>
                <Select
                  name="pais"
                  fullWidth
                  label="País"
                  helperText={ touched.pais && errors.pais ? errors.pais : ""}
                  error={ errors.pais ? true : false}
                  native={true}
                  onChange={async e => {
                    const { value } = e.target;
                    const _regions = await getRegions(value);
                    console.log(CountryRegionData[value][0]);
                    setFieldValue("pais", value);
                    setFieldValue("pais_name", CountryRegionData[value][0]);
                    setFieldValue("departament", CountryRegionData[value][2].split("|")[0].split("~")[0]);
                    setFieldValue("regions", _regions);
                  }}
                  onBlur={handleBlur}
                  value={values.pais}
                  select  
                >
                  {CountryRegionData.map((option, val) => (
                    <option  key={val} value={val}>
                      {option[0]}
                    </option >
                  ))}
                </Select>
            </div>
            <div>
                <TextField 
                    name="firstname"
                    fullWidth
                    label="Nombre"
                    helperText={ touched.firstname && errors.firstname ? errors.firstname : ""}
                    error={ errors.firstname ? true : false}
                    type="text"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.firstname}  
                />
            </div>
            <div>
                <TextField 
                    name="lastname"
                    fullWidth
                    label="Apellido"
                    helperText={ touched.lastname && errors.lastname ? errors.lastname : ""}
                    error={ errors.lastname ? true : false}
                    type="text"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.lastname}  
                />
            </div>
            <div>
                <TextField 
                    name="email"
                    fullWidth
                    label="Correo"
                    helperText={ touched.email && errors.email ? errors.email : ""}
                    error={ errors.email ? true : false}
                    type="email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}  
                />
            </div>
            <div className={classNames(this.props.classes.pt)} >
                <Select 
                  name="departament"
                  fullWidth
                  label="Departamento"
                  native={true}
                  helperText={ touched.departament && errors.departament ? errors.departament : ""}
                  error={ errors.departament ? true : false}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.departament}
                  select  
                >
                  
                  {values.regions &&
                    values.regions.map(r => (
                      <option key={r.split("~")[0]} value={r.split("~")[0]}>
                        {r.split("~")[0]}
                      </option>
                    ))}
                </Select>
                
            </div>
            <div>
                <TextField 
                    name="phone_number"
                    fullWidth
                    label="Teléfono"
                    helperText={ touched.phone_number && errors.phone_number ? errors.phone_number : ""}
                    error={ errors.phone_number ? true : false}
                    type="text"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.phone_number}  
                />
            </div>
            <div>
                <TextField 
                    name="age"
                    fullWidth
                    label="Edad"
                    helperText={ touched.age && errors.age ? errors.age : ""}
                    error={ errors.age ? true : false}
                    type="text"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.age}  
                />
            </div>

            <div className={classNames("mt-5")}>

              <Button
                className={classNames(this.props.classes.otro)} 
                type="button"
                variant="contained"
                onClick={handleReset}
                disabled={!dirty || isSubmitting}
              >
                Limpiar
              </Button>

              <Button
                type="submit"
                variant="contained"  
                color="primary"
                disabled={isSubmitting}
              >
                Inscríbete ahora
              </Button>
            </div>

            <div className={classNames("mt-5")}>

            { this.state.submitForm      
            ? <TrancisionAlert /> 
            :  null
            }
            </div>
          </Form>
        </Card>
        )}
      />
    )

    /*
    return (

      
      
    );
    */
  }
};


ContactFormik.propTypes = {
    width: PropTypes.string.isRequired
};
  
  
export default withStyles(styles, { withTheme: true })(
  withWidth()(ContactFormik)
);


