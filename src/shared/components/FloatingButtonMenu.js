import React, { Component, PropTypes } from 'react';
import classNames from "classnames";
import { Card, Typography, withWidth, withStyles, isWidthUp, Select, TextField, Button, Avatar  } from "@material-ui/core";

import IconButton from '@material-ui/core/IconButton';
import facebookIcon from '../../logged_out/dummy_data/images/facebook.png';
import tiktokIcon from '../../logged_out/dummy_data/images/tiktok.png';
import youTubeIcon from '../../logged_out/dummy_data/images/youtubr.png';
import linkedInIcon from '../../logged_out/dummy_data/images/linkedin.png';
import instagramIcon from '../../logged_out/dummy_data/images/instagram.png';
import WhatsApp from "@material-ui/icons/WhatsApp";

const styles = theme => ({
    socialButtons: {
        position: 'fixed',
        top: "160px",
        zIndex: "100",
        width: "50px",
        [theme.breakpoints.down("xs")]: {
            top: "100px",
        },
    },
    bgGreen: {
        backgroundColor: "#4dc247",
        width: theme.spacing(6),
        height: theme.spacing(6),
        boxShadow: theme.shadows[4]
    },
    wsButtons: {
        position: 'fixed',
        bottom: "70px",
        right: "10px",
        zIndex: "100",
        width: "50px",
        [theme.breakpoints.down("xs")]: {
            //top: "100px",
        },
    },
    facebookIcon: {
        color: "#087AEA",

 
    },
    wsIcon: {
        color: "#FFFFFF",
        fontSize: "30px",
 
    },
    twitterIcon: {
        color: "#2DAAE2",
        
 
    },
    youTubeIcon: {
        color: "#FF0000",
        
    },
    linkedInIcon: {
        color: "#01669E",
        
    },
    instagramIcon: {
        color: "#EA3E66",
        
    },
    format1: {
       
    },
    format2: {
        //background: 'linear-gradient(45deg, #FEDC3D 30%, #FFFFFF 90%)',
        //border: 1,
        padding: theme.spacing(0.4),
        boxShadow: theme.shadows[4],
        marginLeft: theme.spacing(2),
        marginBottom: theme.spacing(0.5),
        [theme.breakpoints.down("xs")]: {
            //background: 'inherit',
            
            marginLeft: theme.spacing(0.5),
            marginBottom: theme.spacing(1.5),
            //borderRadius: "0px 3px 3px 0px",
            padding: theme.spacing(0.1),
        },
       
    },
    socialIcons: {
        width: "35px",
        [theme.breakpoints.down("xs")]: {
            width: "35px",
            
        },
    }
});

class TestComponent extends Component {

    constructor (props) {
        super(props);
      
      }

    render () {

        return (
        <>
        <div className={classNames(this.props.classes.wsButtons)}>
            <Avatar className={classNames(this.props.classes.bgGreen)}>
                <IconButton 
                    className={classNames("")} 
                    color="secondary"
                    href="https://api.whatsapp.com/send?phone=+51920011775&text=Hola, quisiera más información." target="_blank"
                >
                    <WhatsApp className={classNames(this.props.classes.wsIcon)}  />
                </IconButton>
            </Avatar>
            
        </div>
        <div className={classNames(this.props.classes.socialButtons)}>
            <IconButton 
                className={classNames(this.props.classes.format2)} 
                color="secondary"
                href="https://www.facebook.com/elteacherchristian/" target="_blank"
            >
                <img className={this.props.classes.socialIcons} src={facebookIcon} />
            </IconButton>
            <IconButton 
                className={classNames(this.props.classes.format2)} 
                color="secondary"
                href="https://vm.tiktok.com/J86yJ9o/" target="_blank" 
            >
                <img className={this.props.classes.socialIcons} src={tiktokIcon} />
            </IconButton>
            <IconButton 
                className={classNames(this.props.classes.format2)} 
                color="secondary" 
                href="https://www.youtube.com/channel/UCKA6x2vNYc_dfueZqxZgpIw" target="_blank"
            >
                <img className={this.props.classes.socialIcons} src={youTubeIcon} />
            </IconButton>
            <IconButton 
                className={classNames(this.props.classes.format2)} 
                color="secondary" 
                href="https://www.linkedin.com/in/christian-yaranga-743ba3182/" target="_blank"
            >
                <img className={this.props.classes.socialIcons} src={linkedInIcon} />
            </IconButton>
            <IconButton 
                className={classNames(this.props.classes.format2)} 
                color="secondary" 
                href="https://www.instagram.com/soychristianyaranga/" target="_blank"
            >
                <img className={this.props.classes.socialIcons} src={instagramIcon} />
            </IconButton>
        </div>
        </>
        )
    }
}

// export
export default withStyles(styles, { withTheme: true })(
    withWidth()(TestComponent)
);